// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "collision.h"

bool
collision_util_compute_impact (CollisionType type1, CollisionType type2)
{
	if (type1 == COLLISION_NONE || type2 == COLLISION_NONE)
		return false;

	else if (type1 == COLLISION_FULL || type2 == COLLISION_FULL)
		return true;

	else if (type1 == type2)
		return false;

	else
		return !!(type1 | type2);
}
