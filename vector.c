// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "vector.h"

Vec2
vec2_add (Vec2 a, Vec2 b)
{
	Vec2 ret;

	ret.x = a.x + b.x;
	ret.y = a.y + b.y;

	return ret;
}

Vec2
vec2_sub (Vec2 a, Vec2 b)
{
	Vec2 ret;

	ret.x = a.x - b.x;
	ret.y = a.y - b.y;

	return ret;
}

Vec2
vec2_mult (Vec2 vec, int i)
{
	Vec2 ret;

	ret.x = vec.x * i;
	ret.y = vec.y * i;

	return ret;
}

bool
vec2_eq (Vec2 a, Vec2 b)
{
	return a.x == b.x && a.y == b.y;
}
