// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

#include "viewport.h"
#include "tile.h"
#include "tileset.h"
#include "player-tile.h"
#include "map.h"
#include "input.h"
#include "util.h"
#include "engine-signal.h"

typedef struct Engine Engine;
struct Engine
{
	SDL_Renderer *renderer;
	SDL_Window *window;
	GPtrArray *view_ports;	/* owned */
	EngineSignalStack *signals;
};

Engine *engine_new (SDL_Renderer *renderer, SDL_Window *window);
void engine_destroy (Engine *self);

void engine_add_view_port (Engine *self, ViewPort *vp);
void engine_remove_view_port (Engine *self, ViewPort *vp);
void engine_update_view_port_dimensions (Engine *self, guint index);
void engine_update_view_ports (Engine *self);
void engine_draw_tile (Engine *self, Tile *tile);
void engine_draw_map (Engine *self, guint view_port_index, Map *map, TileSet *set);
void engine_move_map_to_pos (Engine *self, guint view_port_index, Map *map, Vec2 new_map_pos, int vel);
void engine_move_map (Engine *self, guint view_port_index, Map *map, Vec2 vec, int vel);
ViewPort *engine_get_view_port (Engine *self, guint view_port_index);
bool engine_check_mt_with_map_tile_collision (Engine *self, guint view_port_index, Map *map, MovableTile *mt, Vec2 vec);
Vec2 engine_get_tile_map_pos (Engine *self, guint view_port_index, Map *map, Tile *tile);
void engine_set_tile_map_pos (Engine *self, guint view_port_index, Map *map, Tile *tile, Vec2 new_map_vec);
void engine_set_sprite_tile_map_pos (Engine *self, guint view_port_index, Map *map, SpriteTile *st, Vec2 new_map_vec);
Vec2 engine_get_center_map_pos (Engine *self, guint view_port_index, Map *map);
void engine_center_vp_around_player_tile (Engine *self, guint view_port_index, Map *map, PlayerTile *pt);
bool engine_is_tile_visible_in_view_port (Engine *self, Vec2 tile_map_pos, guint view_port_index);
void engine_move_player_tile_or_scroll_map (Engine *self, guint view_port_index, Map *map, PlayerTile *pt, Vec2 vec, int vel);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Engine, engine_destroy)
