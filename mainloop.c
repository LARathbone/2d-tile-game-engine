// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "mainloop.h"

#define VP_SCROLL_THRESHOLD 4

static TileSet *TEST_bg_set;
static SpriteTileSet *TEST_sprite_set;

static PlayerTile *TEST_pt;
static PlayerTile *TEST_pt2;

static SpriteTileSet *TEST_npc_sprite_set;
static SpriteTile *TEST_npc;

static GPtrArray *TEST_matrix;
static Map *TEST_map;
static Player *TEST_player1;
static Player *TEST_player2;

static GameState *global_state;

// TEST
static void handle_keyup (SDL_Scancode scancode)
{
	InputTuple *tuple = settings_get_input_tuple_for_scancode (global_state->settings, scancode);
	Player *player;

	if (!tuple) return;

	player = game_state_get_player (global_state, tuple->player_num);
	if (!player || !player->player_tile)
		return;

	if (tuple->action > DIR_NONE && tuple->action < ACTION_0)
		game_state_set_input_action (global_state, player, DIR_NONE);
}

// TEST
static void handle_keydown (SDL_Scancode scancode)
{
	InputTuple *tuple;
	Player *player;

	tuple = settings_get_input_tuple_for_scancode (global_state->settings, scancode);
	if (!tuple) return;

	player = game_state_get_player (global_state, tuple->player_num);
	if (!player) return;

	game_state_set_input_action (global_state, player, tuple->action);
}

static void
TEST_setup_players_if_required (Engine *engine)
{
	if (!TEST_pt)
	{
		TEST_sprite_set = SPRITE_TILESET(tileset_create_from_file ("sprite/cecil.ini"));

		TEST_pt = player_tile_new (TEST_sprite_set, 48, 48);
		// TEST - TODO - this should be fetchable from the tileset/ini file etc.
//#if 0
		TEST_pt2 = player_tile_new (TEST_sprite_set, 48, 48);
		engine_set_tile_map_pos (engine, 0, TEST_map, TILE(TEST_pt2), engine_get_center_map_pos (engine, 0, TEST_map));
//#endif

		engine_set_tile_map_pos (engine, 0, TEST_map, TILE(TEST_pt), engine_get_center_map_pos (engine, 0, TEST_map));

		TEST_player1 = player_new ();
		TEST_player1->player_tile = TEST_pt;
		game_state_add_player (global_state, TEST_player1);
		view_port_add_player (engine_get_view_port (engine, 0), TEST_player1);
//#if 0
		TEST_player2 = player_new ();
		TEST_player2->player_tile = TEST_pt2;
		game_state_add_player (global_state, TEST_player2);
		view_port_add_player (engine_get_view_port (engine, 0), TEST_player2);
//#endif
	}
}

static void
TEST_setup_test_npc_if_required (Engine *engine)
{
	if (!TEST_npc)
	{
		TEST_npc_sprite_set = SPRITE_TILESET(tileset_create_from_file ("sprite/soccer.ini"));

		TEST_npc = sprite_tile_new (TEST_npc_sprite_set, 48, 48);
		engine_set_sprite_tile_map_pos (engine, 0, TEST_map, TEST_npc, (Vec2){3,3});
	}
}

static void
TEST_setup_stuff_if_required (Engine *engine)
{
	if (! TEST_map)
	{
		int w_by_tiles, h_by_tiles;

		TEST_bg_set = tileset_create_from_file ("outdoor/outdoor.ini");
		TEST_matrix = map_util_create_matrix_from_file ("test.map", &w_by_tiles, &h_by_tiles);
		TEST_map = map_new (TEST_matrix, w_by_tiles, h_by_tiles);
	}

}

#define TEST_VEL 4
static void
TEST_render_player_tiles (Engine *engine, guint view_port_index, Map *map)
{
	ViewPort *vp = engine->view_ports->pdata[view_port_index];

	for (guint player_num = 0; player_num < vp->players->len; ++player_num)
	{
		Vec2 mt_vec = VEC2_STATIC;
		bool coll = false;
		Player *player = vp->players->pdata[player_num];
		PlayerTile *pt = player->player_tile;
		Vec2 pt_tile_map_pos = {0};

		engine_draw_tile (engine, TILE(pt));

		if (MOVABLE_TILE(pt)->move_state != MOVABLE_TILE_MOVING &&
				vp->map_move_state != VIEWPORT_MAP_MOVING)
		{
			player->input->action = game_state_get_input_action (global_state, player);
		}

		switch (player->input->action)
		{
			case DIR_LEFT:
				mt_vec = VEC2_LEFT;
				break;
			case DIR_RIGHT:
				mt_vec = VEC2_RIGHT;
				break;
			case DIR_UP:
				mt_vec = VEC2_UP;
				break;
			case DIR_DOWN:
				mt_vec = VEC2_DOWN;
				break;
			default:
				break;
		}

		if (! vec2_eq (mt_vec, VEC2_STATIC))
		{
			sprite_tile_set_direction (SPRITE_TILE(pt), mt_vec);
		}

		/* Handle animations */

		sprite_tile_refresh_anim_speed (SPRITE_TILE(pt));
		game_state_cycle_sprite_tile_animation (global_state, SPRITE_TILE(pt));

		/* Move players if required */

		/* If we detect a collision or the direction vector is static, don't move ... */

		coll = engine_check_mt_with_map_tile_collision (engine, view_port_index, TEST_map, MOVABLE_TILE(pt), mt_vec);

		if (coll)
			continue;

		if (vec2_eq (mt_vec, VEC2_STATIC))
			continue;

		/* ... Otherwise, proceed. */

		/* more than one player in the viewport - don't need to scroll, just move player tile */

		if (vp->players->len > 1)
		{
			movable_tile_move (MOVABLE_TILE(pt), mt_vec, TEST_VEL);

			pt_tile_map_pos = engine_get_tile_map_pos (engine, view_port_index, TEST_map, TILE(pt));

			if (! engine_is_tile_visible_in_view_port (engine, pt_tile_map_pos, view_port_index))
			{
				ViewPort *new_vp = view_port_new (vp->tile_w, vp->tile_h);

				engine_add_view_port (engine, new_vp);
				view_port_remove_player (vp, player);
				view_port_add_player (new_vp, player);
				break;
			}
			continue;
		}

		/* one player in the viewport - may need to move tile or scroll map */

		engine_move_player_tile_or_scroll_map (engine, view_port_index, TEST_map, pt, mt_vec, TEST_VEL);

		/* Merge viewports if possible */

		pt_tile_map_pos = engine_get_tile_map_pos (engine, view_port_index, TEST_map, TILE(pt));

		/* see if the player has entered someone else's viewport */
		for (guint i = 0; i < engine->view_ports->len; ++i)
		{
			ViewPort *tmp = g_ptr_array_index (engine->view_ports, i);

			if (tmp == vp) continue;

			if (! view_port_check_player (tmp, player) && engine_is_tile_visible_in_view_port (engine, pt_tile_map_pos, i))
			{
				Vec2 map_vec = engine_get_tile_map_pos (engine, view_port_index, map, TILE(pt));

				view_port_add_player (tmp, player);
				view_port_remove_player (vp, player);
				engine_remove_view_port (engine, vp);

				engine_set_tile_map_pos (engine, i, map, TILE(pt), map_vec);

				return;
			}
		}
	}
}
#undef TEST_VEL

static void
TEST_handle_events (void)
{
	SDL_Event e;

	while (SDL_PollEvent (&e) > 0)
	{
		switch (e.type)
		{
			case SDL_KEYDOWN:
				// TEST
				handle_keydown (e.key.keysym.scancode);
				break;

			case SDL_KEYUP:
				// TEST
				handle_keyup (e.key.keysym.scancode);
				break;

			case SDL_WINDOWEVENT:
				switch (e.window.event)
				{
					case SDL_WINDOWEVENT_RESIZED:
					case SDL_WINDOWEVENT_SIZE_CHANGED:
						// TODO if needed
						break;
					default:
						break;
				}
				break;

			case SDL_QUIT:
				global_state->quit_request = true;
				break;

			default:
				break;
		}
	}
}

static void
handle_view_port_added (Engine *engine)
{
	engine_update_view_ports (engine);

	for (guint i = 0; i < engine->view_ports->len; ++i)
	{
		ViewPort *vp = g_ptr_array_index (engine->view_ports, i);
		Player *cont_p = view_port_get_controlling_player (vp);
		PlayerTile *pt = NULL;

		if (cont_p)
		{
			pt = cont_p->player_tile;
			engine_center_vp_around_player_tile (engine, i, TEST_map, pt);
		}
	}
}

static void
handle_view_port_removed (Engine *engine)
{
	Player *cont_p = NULL;

	engine_update_view_ports (engine);

#if 0
	for (guint i = 0; i < engine->view_ports->len; ++i)
	{
		ViewPort *vp = g_ptr_array_index (engine->view_ports, i);
		PlayerTile *pt = NULL;

		cont_p = view_port_get_controlling_player (vp);

		if (cont_p)
		{
			pt = cont_p->player_tile;
			engine_center_vp_around_player_tile (engine, i, TEST_map, pt);
		}
	}

	if (! cont_p)
	{
		ViewPort *vp = g_ptr_array_index (engine->view_ports, 0);
		Player *player = g_ptr_array_index (vp->players, 0);
		PlayerTile *pt = player->player_tile;

		// TEST
		if (pt)
			engine_center_vp_around_player_tile (engine, 0, TEST_map, pt);
		else
			g_debug ("%s: Couldn't find any player to center tile around!", __func__);
	}
#endif
}

static void
handle_signals (Engine *engine)
{
	for (guint i = 0; i < engine->signals->queue->len; ++i)
	{
		switch (g_array_index (engine->signals->queue, int, i))
		{
			case ENGSIG_VIEW_PORT_ADDED:
				handle_view_port_added (engine);
				break;

			case ENGSIG_VIEW_PORT_REMOVED:
				handle_view_port_removed (engine);
				break;

			default:
				break;
		}
	}
	engine_signal_stack_clear (engine->signals);
}

#ifdef DEBUG
inline static void
debug_print_fps (void)
{
	static guint counter;

	++counter;
	if (counter >= 100) counter = 0;

	if (counter == 0)
		g_debug ("Average FPS: %f", game_state_get_average_fps (global_state));
}
#endif

void
MainLoop (GameState *state)
{
	Engine *engine = NULL;	/* shorthand */

	if (! global_state)
		global_state = state;

	g_assert (state->engine != NULL);
	engine = state->engine;

	timer_start (global_state->timer);

	TEST_setup_stuff_if_required (engine);

	while (! global_state->quit_request)
	{
		guint64 ticks = timer_get_ticks (global_state->timer);
		g_assert (ticks > 0);

#ifdef DEBUG
		debug_print_fps ();
#endif

		TEST_handle_events ();

		SDL_RenderClear (engine->renderer);

		/* <DO STUFF> */

		handle_signals (engine);

		engine_update_view_ports (engine);

		TEST_setup_players_if_required (engine);
		TEST_setup_test_npc_if_required (engine);

		for (guint i = 0; i < engine->view_ports->len; ++i)
		{
			ViewPort *vp = g_ptr_array_index (engine->view_ports, i);

			SDL_RenderSetViewport (engine->renderer, &vp->pixel_pos);
			engine_draw_map (engine, i, TEST_map, TEST_bg_set);
			TEST_render_player_tiles (engine, i, TEST_map);
			SDL_RenderSetViewport (engine->renderer, NULL);
		}

		/* </DO STUFF> */

		SDL_RenderPresent (engine->renderer);
		++global_state->counted_frames;
	}
}
