// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "input.h"

Input *
input_new (void)
{
	Input *self = g_new0 (Input, 1);
	return self;
}

void
input_destroy (Input *self)
{
	g_free (self);
}

/* --- */

InputTuple *
input_tuple_new (int player_num, InputAction action)
{
	InputTuple *self = g_new0 (InputTuple, 1);
	self->player_num = player_num;
	self->action = action;
	return self;
}

void
input_tuple_destroy (InputTuple *self)
{
	g_free (self);
}
