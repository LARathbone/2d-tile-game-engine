# ARCHIVED

This project is archived and will no longer be maintained.

Please see its spiritual successor: https://gitlab.com/LARathbone/spritesheet-game-engine

# 2D Tile Game Engine (WIP)

This is VERY much a work in progress and is in very early stages of
development.  It is an attempt to create a 2D tile-based game engine with
sprites and movement similar to Final Fantasy IV-V, but will ultimately play
more like a light rogue-like, similar to Fatal Labyrinth for Genesis, but with
local multiplayer, and in realtime.

Currently all it does is allows for the control of up to 2 player sprites (see
settings.ini for the controls; by default it is up, down, left and right arrow
keys for player 1, and w,a,s,d for player 2.

Written in C11.

## Dependencies:

- GCC or Clang; since extensions are used other compilers are not supported.
- SDL2 >= 2.0.18
- SDL2\_image
- glib2  >= 2.72

## Copyright, etc.

Copyright (C) 2023 Logan Rathbone, Province of Ontario, Canada

Licensed under the "WTFPL", version 2:

```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004
 
Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
```
