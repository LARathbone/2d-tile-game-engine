// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

typedef enum
{
	DIR_NONE,
	DIR_LEFT,
	DIR_RIGHT,
	DIR_DOWN,
	DIR_UP,
	ACTION_0,
	ACTION_1,
	ACTION_2,
	ACTION_3,
	ACTION_4,
	ACTION_5
} InputAction;

typedef struct Input Input;
struct Input
{
	InputAction action;
};

Input *input_new (void);
void input_destroy (Input *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Input, input_destroy)

typedef struct
{
	int player_num;
	InputAction action;
} InputTuple;

InputTuple *input_tuple_new (int player_num, InputAction action);
void input_tuple_destroy (InputTuple *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (InputTuple, input_tuple_destroy)
