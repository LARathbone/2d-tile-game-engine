// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "map.h"

#define MAP_MAX_WIDTH_IN_TILES 4096
#define MAP_FILE_LINE_MAX_CHAR_LEN (MAP_MAX_WIDTH_IN_TILES * 2)

/* MapNode */

MapNode *
map_node_new (void)
{
	MapNode *self = g_new0 (MapNode, 1);

	self->set_index = -1;
	self->sprites = g_ptr_array_new ();

	return self;
}

void
map_node_destroy (MapNode *self)
{
	g_clear_pointer (&self->sprites, g_ptr_array_unref);
	g_free (self);
}

void
map_node_add_sprite_tile (MapNode *self, SpriteTile *st)
{
	g_ptr_array_add (self->sprites, st);
}

void
map_node_remove_sprite_tile (MapNode *self, SpriteTile *st)
{
	g_ptr_array_remove_fast (self->sprites, st);
}

/* Map */

Map *
map_new (GPtrArray *matrix, int w_by_tiles, int h_by_tiles)
{
	Map *self;

	g_assert (matrix && w_by_tiles && h_by_tiles);

	self = g_new0 (Map, 1);
	self->w_by_tiles = w_by_tiles;
	self->h_by_tiles = h_by_tiles;
	map_set_matrix (self, matrix);

	return self;
}

void
map_set_matrix (Map *self, GPtrArray *matrix)
{
	g_assert (matrix->len == self->w_by_tiles * self->h_by_tiles);

	g_clear_pointer (&self->matrix, g_ptr_array_unref);
	self->matrix = matrix;
}

MapNode *
map_get_node_for_pos (Map *self, Vec2 pos)
{
	int pos_1d = util_get_1d_pos_from_2d_pos (pos.x, pos.y, self->w_by_tiles);

	if (pos_1d < 0 || pos_1d >= self->matrix->len)
		return NULL;
	else
		return g_ptr_array_index (self->matrix, util_get_1d_pos_from_2d_pos (pos.x, pos.y, self->w_by_tiles));
}

GPtrArray *
map_util_create_empty_matrix (int w_by_tiles, int h_by_tiles)
{
	GPtrArray *matrix = g_ptr_array_new ();
	guint area = w_by_tiles * h_by_tiles;

	for (int i = 0; i < area; ++i)
	{
		MapNode *node = map_node_new ();
		g_ptr_array_add (matrix, node);
	}

	return matrix;
}

GPtrArray *
map_util_create_matrix_from_file (const char *path, int *wp, int *hp)
{
	GPtrArray *matrix = g_ptr_array_new ();
	FILE *fp;
	char line[MAP_FILE_LINE_MAX_CHAR_LEN];
	int w = 0, h = 0;

	errno = 0;
	fp = fopen (path, "r");
	if (!fp)
	{
		g_autofree char *errmsg = g_strdup_printf ("Failed to open map file. %s\n", errno ? g_strerror (errno) : "");
		FatalError (errmsg);
	}

	for (h = 0, w = 0; fgets (line, MAP_FILE_LINE_MAX_CHAR_LEN, fp) != NULL; ++h)
	{
		char *tok;

		for (w = 0; ((tok = strtok (w ? NULL : line, ",")) != NULL); ++w)
		{
			MapNode *node = map_node_new ();
			int retval;
			int val;

			retval = sscanf (tok, "%d", &val);
			g_assert (retval == 1);
			node->set_index = val;
			g_ptr_array_add (matrix, node);
		}
	}

	g_assert (w*h == (int)(matrix->len));

	*wp = w;
	*hp = h;

	return matrix;
}
