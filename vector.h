// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

#define VEC2_STATIC	((Vec2){.x=0, .y=0})
#define VEC2_UP		((Vec2){.x=0, .y=-1})
#define VEC2_DOWN	((Vec2){.x=0, .y=1})
#define VEC2_LEFT	((Vec2){.x=-1, .y=0})
#define VEC2_RIGHT	((Vec2){.x=1, .y=0})

typedef union
{
	struct {
		int x;
		int y;
	};
	int vals[2];
} Vec2;

Vec2 vec2_add (Vec2 a, Vec2 b);
Vec2 vec2_sub (Vec2 a, Vec2 b);
Vec2 vec2_mult (Vec2 vec, int i);
bool vec2_eq (Vec2 a, Vec2 b);
