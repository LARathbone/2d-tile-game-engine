// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "tile.h"

typedef struct PlayerTile PlayerTile;
struct PlayerTile
{
	SpriteTile parent;
};

PlayerTile *player_tile_new (SpriteTileSet *set, int tile_w, int tile_h);
void player_tile_destroy (PlayerTile *self);
