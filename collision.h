// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

typedef enum
{
	COLLISION_INVALID =	-1,
	COLLISION_NONE = 	0,
	COLLISION_LEFT =	1 << 0,
	COLLISION_RIGHT = 	1 << 1,
	COLLISION_UP =		1 << 2,
	COLLISION_DOWN =	1 << 3,
	COLLISION_FULL = 	0xF
} CollisionType;

bool collision_util_compute_impact (CollisionType type1, CollisionType type2);
