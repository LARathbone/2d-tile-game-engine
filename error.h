// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

void FatalError (const char *errmsg);
void PrintError (const char *errmsg);
