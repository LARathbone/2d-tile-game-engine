// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include <stdbool.h>
#include <SDL.h>
#include <SDL_image.h>
#include <glib.h>

#include "sdl-autocleanups.h"
#include "vector.h"
#include "error.h"
