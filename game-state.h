// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "input.h"
#include "player.h"
#include "settings.h"
#include "timer.h"
#include "engine.h"

typedef struct GameState GameState;
struct GameState
{
	Engine *engine;			/* owned */
	GPtrArray *players;		/* owned */
	GHashTable *input_hash;
	bool quit_request;
	Settings *settings;
	Timer *timer;
	guint64 counted_frames;
	double avg_fps;
};

GameState *game_state_new (Settings *settings);
void game_state_destroy (GameState *self);
void game_state_add_player (GameState *self, Player *player);
void game_state_remove_player (GameState *self, Player *player);
InputAction game_state_get_input_action (GameState *self, Player *player);
void game_state_set_input_action (GameState *self, Player *player, InputAction action);
Player *game_state_get_player (GameState *self, int player_num);
double game_state_get_average_fps (GameState *self);
void game_state_cycle_sprite_tile_animation (GameState *self, SpriteTile *st);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GameState, game_state_destroy)
