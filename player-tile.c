// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "player-tile.h"

PlayerTile *player_tile_new (SpriteTileSet *set, int tile_w, int tile_h)
{
	PlayerTile *self;

	g_assert (set && tile_w && tile_h);

	self = g_new0 (PlayerTile, 1);
	sprite_tile_constructor (&self->parent, set, tile_w, tile_h);

	// TODO do other stuff

	return self;
}

void
player_tile_destroy (PlayerTile *self)
{
	/* Chain up */
	sprite_tile_destroy (&self->parent);
}
