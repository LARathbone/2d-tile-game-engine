// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "input.h"
#include "player-tile.h"

typedef struct
{
	Input *input;
	PlayerTile *player_tile;
} Player;

Player *player_new (void);
void player_destroy (Player *self);
