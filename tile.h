// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "tileset.h"

#define TILE(X) ((Tile *)(X))

typedef struct Tile Tile;
struct Tile
{
	Vec2 pos;
	int set_index;

	TileSet *tileset;
	SDL_Rect pixel_pos;
};

Tile *tile_new (TileSet *set, int tile_w, int tile_h);
void tile_constructor (Tile *self, TileSet *set, int tile_w, int tile_h);
void tile_destroy (Tile *self);
void tile_set_pos (Tile *self, Vec2 new_pos);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Tile, tile_destroy)

/* --- */

#define MOVABLE_TILE(X) ((MovableTile *)(X))

typedef enum
{
	MOVABLE_TILE_STATIC,
	MOVABLE_TILE_MOVING
} MovableTileMoveState;

typedef struct MovableTile MovableTile;
struct MovableTile
{
	Tile parent;

	MovableTileMoveState move_state;
	SDL_Rect last_pixel_pos;
};

MovableTile *movable_tile_new (TileSet *set, int tile_w, int tile_h);
void movable_tile_constructor (MovableTile *self, TileSet *set, int tile_w, int tile_h);
void movable_tile_destroy (MovableTile *self);
void movable_tile_move_to_pos (MovableTile *self, Vec2 new_pos, int vel);
void movable_tile_move (MovableTile *self, Vec2 vec, int vel);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (MovableTile, movable_tile_destroy)

/* --- */

#define SPRITE_TILE(X) ((SpriteTile *)(X))

typedef struct SpriteTile SpriteTile;
struct SpriteTile
{
	MovableTile parent;

	Vec2 dir_vec;
	int last_anim_frame;
	int anim_speed;
};

SpriteTile *sprite_tile_new (SpriteTileSet *set, int tile_w, int tile_h);
void sprite_tile_constructor (SpriteTile *self, SpriteTileSet *set, int tile_w, int tile_h);
void sprite_tile_destroy (SpriteTile *self);
void sprite_tile_set_direction (SpriteTile *self, Vec2 dir_vec);
void sprite_tile_refresh_anim_speed (SpriteTile *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (SpriteTile, sprite_tile_destroy)
