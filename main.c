// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "common-headers.h"

#include "engine.h"
#include "mainloop.h"

#define DEFAULT_WINDOW_WIDTH	640
#define DEFAULT_WINDOW_HEIGHT	480
#define DEFAULT_TILE_WIDTH		48
#define DEFAULT_TILE_HEIGHT		48
#define INI_FILE				"settings.ini"

int
main (int argc, char *argv[])
{
	g_autoptr(GameState) state = NULL;
	Engine *engine = NULL;
	Settings *settings = NULL;
	// TEST - wouldn't want to hardcode 1 viewport
	ViewPort *vp = NULL;
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;

	if (SDL_Init (SDL_INIT_VIDEO) < 0) FatalError (NULL);

	window = SDL_CreateWindow ("SDL2 Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE);
	if (!window) FatalError (NULL);

	renderer = SDL_CreateRenderer (window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!renderer) FatalError (NULL);

	settings = settings_new (INI_FILE);

	engine = engine_new (renderer, window);
	vp = view_port_new (DEFAULT_TILE_WIDTH, DEFAULT_TILE_HEIGHT);

	engine_add_view_port (engine, vp);

	state = game_state_new (settings);
	state->engine = engine;

	MainLoop (state);

	return 0;
}
