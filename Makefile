CC ?= gcc

#DEBUG = -fsanitize=address,undefined -g
DEBUG ?= -g

ifdef DEBUG
DEBUG_FLAGS = -DDEBUG
endif

LOCAL_CFLAGS ?=

CFLAGS = -Wall -Wimplicit-fallthrough=2 -Wno-missing-braces -Werror=implicit -Werror=return-type $(shell pkg-config --libs --cflags sdl2 SDL2_image glib-2.0) $(DEBUG) $(DEBUG_FLAGS) $(LOCAL_CFLAGS)

OBJECTS = collision.o engine.o engine-signal.o error.o game-state.o input.o mainloop.o map.o player.o player-tile.o settings.o tile.o tileset.o timer.o util.o vector.o viewport.o

default: main

main: $(OBJECTS)

test-vec: vector.o

test: test-vec
	./test-vec

clean:
	rm -f *.o main test-vec

.PHONY: clean test
