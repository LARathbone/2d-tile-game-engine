// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

typedef struct Timer Timer;
struct Timer
{
	bool started;
	bool paused;

	/*< private >*/
	guint64 start_ticks;
	guint64 paused_ticks;
};

Timer *timer_new (void);
void timer_destroy (Timer *self);
void timer_start (Timer *self);
void timer_pause (Timer *self);
guint64 timer_get_ticks (Timer *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Timer, timer_destroy)
