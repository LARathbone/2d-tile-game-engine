// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

int util_get_1d_pos_from_2d_pos (int xpos, int ypos, int w);
void util_get_2d_pos_from_2d_pos (int pos, int w, int *xpos, int *ypos);
