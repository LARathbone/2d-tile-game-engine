// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "timer.h"

Timer *
timer_new (void)
{
	Timer *self = g_new0 (Timer, 1);

	return self;
}

void
timer_destroy (Timer *self)
{
	g_free (self);
}

void
timer_start (Timer *self)
{
	self->started = true;
	self->paused = false;
	self->start_ticks = SDL_GetTicks64 ();
	self->paused_ticks = 0;
}

void
timer_stop (Timer *self)
{
	self->started = false;
	self->paused = false;
	self->start_ticks = 0;
	self->paused_ticks = 0;
}

void
timer_pause (Timer *self)
{
	if (self->started && !self->paused)
	{
		self->paused = true;

		self->paused_ticks = SDL_GetTicks64() - self->start_ticks;
		self->start_ticks = 0;
	}
}

guint64
timer_get_ticks (Timer *self)
{
    guint64 ticks = 0;

	if (self->started)
	{
		if (self->paused)
			ticks = self->paused_ticks;
		else
			ticks = SDL_GetTicks64() - self->start_ticks;
	}

	return ticks;
}
