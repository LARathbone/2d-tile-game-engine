// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "game-state.h"

// FIXME - probably wrong approach - must be a better way of fetching it as an initial value - eventually it will be calculated of course.
#define DEFAULT_FPS 60

GameState *
game_state_new (Settings *settings)
{
	GameState *self = g_new0 (GameState, 1);
	
	self->players = g_ptr_array_new_with_free_func ((GDestroyNotify)player_destroy);
	self->input_hash = g_hash_table_new_full (NULL, NULL, NULL, (GDestroyNotify)input_destroy);
	self->timer = timer_new ();
	self->settings = settings;
	self->avg_fps = DEFAULT_FPS;

	return self;
}

void
game_state_destroy (GameState *self)
{
	g_clear_pointer (&self->settings, settings_destroy);
	g_clear_pointer (&self->players, g_ptr_array_unref);
	g_clear_pointer (&self->input_hash, g_hash_table_unref);
	g_clear_pointer (&self->timer, timer_destroy);
	g_free (self);
}

void
game_state_add_player (GameState *self, Player *player)
{
	Input *raw_input = input_new ();

	g_ptr_array_add (self->players, player);
	g_hash_table_insert (self->input_hash, player, raw_input);
}

void
game_state_remove_player (GameState *self, Player *player)
{
	g_ptr_array_remove (self->players, player);
	g_hash_table_remove (self->input_hash, player);
}

InputAction
game_state_get_input_action (GameState *self, Player *player)
{
	Input *raw_input = g_hash_table_lookup (self->input_hash, player);

	return raw_input->action;
}

void
game_state_set_input_action (GameState *self, Player *player, InputAction action)
{
	Input *raw_input = g_hash_table_lookup (self->input_hash, player);

	raw_input->action = action;
}

/* Convenience function to fetch a player using natural numbers so you don't have to count from 0 every time.
 */
Player *
game_state_get_player (GameState *self, int player_num)
{
	return g_ptr_array_index (self->players, player_num-1);
}


double
game_state_get_average_fps (GameState *self)
{
	return self->counted_frames / (timer_get_ticks (self->timer) / 1000.0);
}

void
game_state_cycle_sprite_tile_animation (GameState *self, SpriteTile *st)
{
	TileSetSpriteTiles first_frame = TILESET_SPRITE_TILE_INVALID;
	TileSetSpriteTiles last_frame = TILESET_SPRITE_TILE_INVALID;

	/* Initialize on first run */
	if (! st->last_anim_frame)
		st->last_anim_frame = self->counted_frames;

	tileset_get_sprite_tile_dir_indices (st->dir_vec, &first_frame, &last_frame);

	if (first_frame == TILESET_SPRITE_TILE_INVALID || last_frame == TILESET_SPRITE_TILE_INVALID)
	{
		/* TODO: for now - just return and print a debug message in case we want an automatic - likely just means dir not yet implemented. Later, may want to handle it as an error, since it would mean things are pretty broken.
		 */
		g_warning ("%s: first_frame or last_frame undefined for tileset: %s",
				__func__, TILE(st)->tileset->name);
		return;
	}

	if (self->counted_frames - st->last_anim_frame >= game_state_get_average_fps (self) - st->anim_speed)
	{
		++TILE(st)->set_index;

		if (TILE(st)->set_index > last_frame)
		{
			TILE(st)->set_index = first_frame;
		}

		st->last_anim_frame = self->counted_frames;
	}
}
