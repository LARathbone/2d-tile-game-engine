// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"

typedef enum
{
	ENGSIG_VIEW_PORT_ADDED,
	ENGSIG_VIEW_PORT_REMOVED
} EngineSignalType;

typedef struct EngineSignalStack EngineSignalStack;
struct EngineSignalStack
{
	GArray *queue;
};

EngineSignalStack *engine_signal_stack_new (void);
void engine_signal_stack_destroy (EngineSignalStack *self);
void engine_signal_stack_add (EngineSignalStack *self, EngineSignalType type);
void engine_signal_stack_clear (EngineSignalStack *self);
