// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "tileset.h"

void
tileset_constructor (TileSet *self, int num_tiles)
{
	self->surfaces = g_ptr_array_new_with_free_func ((GDestroyNotify)SDL_FreeSurface);
	self->collision_matrix = g_array_new (TRUE, TRUE, sizeof (CollisionType));

	for (int i = 0; i < num_tiles; ++i)
	{
		CollisionType ct = COLLISION_INVALID;
		g_ptr_array_add (self->surfaces, NULL);
		g_array_append_val (self->collision_matrix, ct);
	}

	g_assert (self->surfaces->len == (guint)num_tiles);
}

TileSet *
tileset_new (int num_tiles)
{
	TileSet *self = g_new0 (TileSet, 1);

	tileset_constructor (self, num_tiles);

	return self;
}

void
tileset_add_img (TileSet *self, guint index, const char *img_path, CollisionType coll_type)
{
	SDL_Surface *sfc;

	g_assert (index < self->surfaces->len);
	g_assert (img_path);
	g_assert (coll_type != COLLISION_INVALID);

	sfc = IMG_Load (img_path);
	if (sfc == NULL) FatalError (NULL);

	self->surfaces->pdata[index] = sfc;
	g_array_index (self->collision_matrix, CollisionType, index) = coll_type;
}

void
tileset_destroy (TileSet *self)
{
	g_clear_pointer (&self->surfaces, g_ptr_array_unref);
	g_clear_pointer (&self->textures, g_ptr_array_unref);
	g_clear_pointer (&self->collision_matrix, g_array_unref);
	g_free (self);
}

inline static int
get_num_tiles_from_tset_type (TileSetType type)
{
	switch (type)
	{
		case TILESET_TYPE_BG_OUTDOOR:
			return TILESET_BG_OUTDOOR_NUM_TILES;
			break;

		case TILESET_TYPE_BG_DUNGEON:
			return TILESET_BG_DUNGEON_NUM_TILES;
			break;

		case TILESET_TYPE_SPRITE:
			return TILESET_SPRITE_NUM_TILES;
			break;

		default:
			g_assert_not_reached ();
			break;
	}
}

inline static TileSetType
get_tileset_type_from_string (const char *str)
{
	if (g_strcmp0 (str, "outdoor") == 0)
	{
		return TILESET_TYPE_BG_OUTDOOR;
	}
	else if (g_strcmp0 (str, "dungeon") == 0)
	{
		return TILESET_TYPE_BG_DUNGEON;
	}
	else if (g_strcmp0 (str, "sprite") == 0)
	{
		return TILESET_TYPE_SPRITE;
	}
	else
	{
		return TILESET_TYPE_INVALID;
	}
}

inline static CollisionType
get_collision_type_from_str (const char *str)
{
	if (g_strcmp0 (str, "none") == 0)
	{
		return COLLISION_NONE;
	}
	else if (g_strcmp0 (str, "full") == 0)
	{
		return COLLISION_FULL;
	}
	else if (g_strcmp0 (str, "left") == 0)
	{
		return COLLISION_LEFT;
	}
	else if (g_strcmp0 (str, "right") == 0)
	{
		return COLLISION_RIGHT;
	}
	else if (g_strcmp0 (str, "up") == 0)
	{
		return COLLISION_UP;
	}
	else if (g_strcmp0 (str, "down") == 0)
	{
		return COLLISION_DOWN;
	}
	else
	{
		return COLLISION_INVALID;
	}
}

#define HANDLE_AND_CLEAR_ERROR  \
	if (local_error) { FatalError (local_error->message); g_clear_pointer (&local_error, g_error_free); }

inline static TileSetSpriteTiles
get_static_sprite_tile_index_for_dir (const char *dir)
{
	TileSetSpriteTiles index = TILESET_SPRITE_TILE_INVALID;

	if (g_strcmp0 (dir, "right") == 0)
	{
		index = TILESET_SPRITE_TILE_RIGHT_STATIC;
	}
	else if (g_strcmp0 (dir, "left") == 0)
	{
		index = TILESET_SPRITE_TILE_LEFT_STATIC;
	}
	else if (g_strcmp0 (dir, "up") == 0)
	{
		index = TILESET_SPRITE_TILE_UP_STATIC;
	}
	else if (g_strcmp0 (dir, "down") == 0)
	{
		index = TILESET_SPRITE_TILE_DOWN_STATIC;
	}
	else
	{
		g_assert_not_reached ();
	}
	return index;
}

void
tileset_get_sprite_tile_dir_indices (Vec2 dir_vec, int *start_index, int *end_index)
{
	TileSetSpriteTiles first_frame = TILESET_SPRITE_TILE_INVALID, last_frame = TILESET_SPRITE_TILE_INVALID;

	if (vec2_eq (dir_vec, VEC2_LEFT))
	{
		first_frame = TILESET_SPRITE_TILE_LEFT_MOVE_1;
		last_frame = TILESET_SPRITE_TILE_LEFT_MOVE_LAST;
	}
	else if (vec2_eq (dir_vec, VEC2_RIGHT))
	{
		first_frame = TILESET_SPRITE_TILE_RIGHT_MOVE_1;
		last_frame = TILESET_SPRITE_TILE_RIGHT_MOVE_LAST;
	}
	else if (vec2_eq (dir_vec, VEC2_UP))
	{
		first_frame = TILESET_SPRITE_TILE_UP_MOVE_1;
		last_frame = TILESET_SPRITE_TILE_UP_MOVE_LAST;
	}
	else if (vec2_eq (dir_vec, VEC2_DOWN))
	{
		first_frame = TILESET_SPRITE_TILE_DOWN_MOVE_1;
		last_frame = TILESET_SPRITE_TILE_DOWN_MOVE_LAST;
	}
	else
	{
		g_autofree char *errmsg = g_strdup_printf ("%s: Invalid direction vector", __func__);
		FatalError (errmsg);
	}

	*start_index = first_frame;
	*end_index = last_frame;
}

static Vec2
get_vec2_dir_from_str (const char *dir)
{
	if (g_strcmp0 (dir, "right") == 0)
		return VEC2_RIGHT;

	else if (g_strcmp0 (dir, "left") == 0)
		return VEC2_LEFT;

	else if (g_strcmp0 (dir, "up") == 0)
		return VEC2_UP;

	else if (g_strcmp0 (dir, "down") == 0)
		return VEC2_DOWN;

	else
		g_assert_not_reached ();
}

static void
get_sprite_tile_dir_indices_from_str (const char *dir, int *start_index, int *end_index)
{
	TileSetSpriteTiles first_frame, last_frame;

	tileset_get_sprite_tile_dir_indices (get_vec2_dir_from_str (dir), &first_frame, &last_frame);

	*start_index = first_frame;
	*end_index = last_frame;
}

// TODO/FIXME - expand this functionality - need animation, etc.
inline static TileSetOutdoorTiles
get_outdoor_tile_type_from_str (const char *str)
{
	TileSetOutdoorTiles type = TILESET_BG_OUTDOOR_INVALID;

	if (g_strcmp0 (str, "grass") == 0)
	{
		type = TILESET_BG_OUTDOOR_GRASS;
	}
	else if (g_strcmp0 (str, "water") == 0)
	{
		type = TILESET_BG_OUTDOOR_WATER;
	}
	else
	{
		g_assert_not_reached ();
	}

	return type;
}

static void
create_outdoor_tileset (TileSet *set, GKeyFile *keyfile, const char *dirname)
{
	g_autoptr(GError) local_error = NULL;
	char *types[] = {"grass", "water", NULL};

	for (int t = 0; types[t] != NULL; ++t)
	{
		CollisionType coll_type = COLLISION_INVALID;
		TileSetOutdoorTiles type = TILESET_BG_OUTDOOR_INVALID;
		g_autofree char *tile_str = NULL;
		g_autofree char *coll_type_str = NULL;

		tile_str = g_key_file_get_value (keyfile, types[t], "frames", &local_error);
		HANDLE_AND_CLEAR_ERROR;
		coll_type_str = g_key_file_get_value (keyfile, types[t], "collision", &local_error);
		HANDLE_AND_CLEAR_ERROR;

		type = get_outdoor_tile_type_from_str (types[t]);
		g_assert (type != TILESET_BG_OUTDOOR_INVALID);

		coll_type = get_collision_type_from_str (coll_type_str);
		g_assert (coll_type != COLLISION_INVALID);

		tileset_add_img (set, type, g_build_filename (dirname, tile_str, NULL), coll_type);
	}
}

// FIXME - this function literally makes me want to puke
static void
create_sprite_tileset (SpriteTileSet *set, GKeyFile *keyfile, const char *dirname)
{
	g_autoptr(GError) local_error = NULL;
	char *dirs[] = {"up", "down", "left", "right", NULL};
	char *actions[] = {"static", "move", NULL};
	g_autofree char *coll_type_str = NULL;
	CollisionType coll_type = COLLISION_INVALID;

	coll_type_str = g_key_file_get_value (keyfile, "tileset", "collision", &local_error);
	HANDLE_AND_CLEAR_ERROR;
	coll_type = get_collision_type_from_str (coll_type_str);

	for (int d = 0; dirs[d] != NULL; ++d)
	{
		for (int a = 0; actions[a] != NULL; ++a)
		{
			g_autofree char *group = g_strdup_printf ("%s %s", dirs[d], actions[a]);

			if (g_strcmp0 (actions[a], "static") == 0)
			{
				g_autofree char *frame_str = NULL;
				TileSetSpriteTiles index = get_static_sprite_tile_index_for_dir (dirs[d]);

				frame_str = g_key_file_get_value (keyfile, group, "frames", &local_error);
				if (local_error)
				{
					g_debug ("%s: %s", __func__, local_error->message);
					g_clear_error (&local_error);
					continue;
				}

				tileset_add_img (TILESET(set), index, g_build_filename (dirname, frame_str, NULL), coll_type);
			}
			else	/* move */
			{
				char *anim_keys[] = {"frames", "speed", NULL};
				int anim_start_index = -1, anim_end_index = -1;
				get_sprite_tile_dir_indices_from_str (dirs[d], &anim_start_index, &anim_end_index);

				for (int k = 0; anim_keys[k] != NULL; ++k)
				{
					if (g_strcmp0 (anim_keys[k], "frames") == 0)
					{
						g_auto(GStrv) frames_str_list = NULL;

						frames_str_list = g_key_file_get_string_list (keyfile, group, anim_keys[k], NULL, &local_error);
						if (local_error)
						{
							g_debug ("%s: Error adding frame: %s", __func__, local_error->message);
							g_clear_error (&local_error);
							continue;
						}

						for (int i = 0; frames_str_list[i] != NULL; ++i)
						{
							int anim_index = anim_start_index + i;
							g_assert (anim_index <= anim_end_index);

							tileset_add_img (TILESET(set), anim_index, g_build_filename (dirname, frames_str_list[i], NULL), coll_type);
						}
					}
					else /* speed */
					{
						int anim_speed = g_key_file_get_integer (keyfile, group, anim_keys[k], NULL);

						for (int i = anim_start_index; i <= anim_end_index; ++i)
							g_array_index (set->anim_speed_matrix, int, i) = anim_speed;
					}
				}
			}
		}
	}
}

TileSet *
tileset_create_from_file (const char *tset_path)
{
	TileSet *set = NULL;
	TileSetType type = TILESET_TYPE_INVALID;
	g_autoptr(GKeyFile) keyfile = NULL;
	g_autoptr(GError) local_error = NULL;
	g_autofree char *dirname = g_path_get_dirname (tset_path);
	g_autofree char *tileset_type_str = NULL;

	g_assert (tset_path);

	keyfile = g_key_file_new ();
	g_key_file_load_from_file (keyfile, tset_path, G_KEY_FILE_NONE, &local_error);
	HANDLE_AND_CLEAR_ERROR;

	tileset_type_str = g_key_file_get_value (keyfile, "tileset", "type", &local_error);
	HANDLE_AND_CLEAR_ERROR;
	type = get_tileset_type_from_string (tileset_type_str);

	switch (type)
	{
//TODO	case TILESET_TYPE_BG_DUNGEON:
//			set = tileset_new (get_num_tiles_from_tset_type (type));
//			create_dungeon_tileset (set, keyfile, dirname);
//			break;

		case TILESET_TYPE_BG_OUTDOOR:
			set = tileset_new (get_num_tiles_from_tset_type (type));
			create_outdoor_tileset (set, keyfile, dirname);
			break;

		case TILESET_TYPE_SPRITE:
			set = TILESET(sprite_tileset_new (get_num_tiles_from_tset_type (type)));
			create_sprite_tileset (SPRITE_TILESET(set), keyfile, dirname);
			break;

		default:
			g_assert_not_reached ();
			break;
	}

	/* Set name - will be NULL if 'name=' in .ini file not provided. */
	set->name = g_key_file_get_string (keyfile, "tileset", "name", NULL);

	return set;
}
#undef HANDLE_AND_CLEAR_ERROR

/* SpriteTileSet */

SpriteTileSet *
sprite_tileset_new (int num_tiles)
{
	SpriteTileSet *self = g_new0 (SpriteTileSet, 1);

	tileset_constructor (&self->parent, num_tiles);

	self->anim_speed_matrix = g_array_new (TRUE, TRUE, sizeof (int));
	for (int i = 0; i < num_tiles; ++i)
	{
		int default_anim_speed = 0;
		g_array_append_val (self->anim_speed_matrix, default_anim_speed);
	}

	return self;
}

void
sprite_tileset_destroy (SpriteTileSet *self)
{
	g_clear_pointer (&self->anim_speed_matrix, g_array_unref);

	/* Chain up */
	tileset_destroy (&self->parent);
}
