// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "engine.h"

/* number of extra tiles to draw off-screen so we can't see new tiles from the cache 'appearing' as we scroll.
 */
#define NUM_EXTRA_TILES 2

Engine *
engine_new (SDL_Renderer *renderer, SDL_Window *window)
{
	Engine *self;

	g_assert (renderer && window);

	self = g_new0 (Engine, 1);
	self->window = window;
	self->renderer = renderer;
	self->view_ports = g_ptr_array_new_with_free_func ((GDestroyNotify)view_port_destroy);
	self->signals = engine_signal_stack_new ();

	return self;
}

void
engine_destroy (Engine *self)
{
	g_clear_pointer (&self->renderer, SDL_DestroyRenderer);
	g_clear_pointer (&self->window, SDL_DestroyWindow);
	g_clear_pointer (&self->view_ports, g_ptr_array_unref);
	g_clear_pointer (&self->signals, engine_signal_stack_destroy);
	g_free (self);
}

void
engine_add_view_port (Engine *self, ViewPort *vp)
{
	g_ptr_array_add (self->view_ports, vp);
	engine_signal_stack_add (self->signals, ENGSIG_VIEW_PORT_ADDED);
}

void
engine_remove_view_port (Engine *self, ViewPort *vp)
{
	g_ptr_array_remove_fast (self->view_ports, vp);
	engine_signal_stack_add (self->signals, ENGSIG_VIEW_PORT_REMOVED);
}

void
engine_update_view_port_dimensions (Engine *self, guint index)
{
	ViewPort *vp = self->view_ports->pdata[index];
	guint num_vps = self->view_ports->len;
	int win_w, win_h;

	SDL_GetWindowSize (self->window, &win_w, &win_h);

	switch (num_vps)
	{
		case 1:
			vp->pixel_pos.w = win_w;
			vp->pixel_pos.h = win_h;
			vp->w_by_tiles = vp->pixel_pos.w / vp->tile_w;
			vp->h_by_tiles = win_h / vp->tile_h;
			break;

		case 2:
			vp->pixel_pos.w = win_w / 2;
			vp->pixel_pos.x = vp->pixel_pos.w * index;
			vp->pixel_pos.h = win_h;
			vp->w_by_tiles = vp->pixel_pos.w / vp->tile_w;
			vp->h_by_tiles = win_h / vp->tile_h;
			break;

		default:
			g_warning ("%s: Number of viewports > 2 NOT IMPLEMENTED", __func__);
			break;
	}
}

void
engine_update_view_ports (Engine *self)
{
	for (guint i = 0; i < self->view_ports->len; ++i)
	{
		engine_update_view_port_dimensions (self, i);
	}
}

/* SDL doesn't allow direct rendering of surfaces, and you need a renderer to create a texture, so we use this extra helper function.
 */
inline static void
generate_tileset_textures_from_surfaces (Engine *self, TileSet *tileset)
{

	tileset->textures = g_ptr_array_new_with_free_func ((GDestroyNotify)SDL_DestroyTexture);

	for (guint i = 0; i < tileset->surfaces->len; ++i)
	{
		SDL_Surface *sfc = tileset->surfaces->pdata[i];
		SDL_Texture *tex = SDL_CreateTextureFromSurface (self->renderer, sfc);
		g_ptr_array_add (tileset->textures, tex);
	}
}

void
engine_draw_tile (Engine *self, Tile *tile)
{
	SDL_Texture *tex;

	if (! tile->tileset->textures)
		generate_tileset_textures_from_surfaces (self, tile->tileset);

	tex = tile->tileset->textures->pdata[tile->set_index];

	SDL_RenderCopy (self->renderer, tex, NULL, &tile->pixel_pos);
}

inline static void
initialize_map_tile_cache (Engine *self, guint view_port_index, Map *map, TileSet *set)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];

	vp->map_tile_cache = g_ptr_array_new_with_free_func ((GDestroyNotify)movable_tile_destroy);

	for (int y = 0; y < map->h_by_tiles; ++y)
	{
		for (int x = 0; x < map->w_by_tiles; ++x)
		{
			MovableTile *mt;
			MapNode *node;
			int map_pos = util_get_1d_pos_from_2d_pos (x, y, map->w_by_tiles);

			if (map_pos >= map->matrix->len)
				break;

			mt = movable_tile_new (set, vp->tile_w, vp->tile_h);

			node = g_ptr_array_index (map->matrix, map_pos);
			TILE(mt)->set_index = node->set_index;

			g_ptr_array_add (vp->map_tile_cache, mt);
		}
	}
}

inline static void
update_and_draw_mt_if_not_moving (Engine *self, guint view_port_index, Map *map, MovableTile *mt, Vec2 map_pos)
{
	if (mt->move_state != MOVABLE_TILE_MOVING)
		engine_set_tile_map_pos (self, view_port_index, map, TILE(mt), map_pos);

	engine_draw_tile (self, TILE(mt));
}

void
engine_draw_map (Engine *self, guint view_port_index, Map *map, TileSet *set)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];

	if (! vp->map_tile_cache)
		initialize_map_tile_cache (self, view_port_index, map, set);

	for (int y = 0 - NUM_EXTRA_TILES; y < vp->h_by_tiles + NUM_EXTRA_TILES; ++y)
	{
		for (int x = 0 - NUM_EXTRA_TILES; x < vp->w_by_tiles + NUM_EXTRA_TILES; ++x)
		{
			MovableTile *mt;
			Vec2 map_vec = vec2_add (vp->map_pos, (Vec2){.x=x, .y=y});
			int tile_map_pos = util_get_1d_pos_from_2d_pos (map_vec.x, map_vec.y, map->w_by_tiles);
			MapNode *node = map_get_node_for_pos (map, map_vec);

			/* Don't draw out-of-bounds tiles */
			if (tile_map_pos < 0 || tile_map_pos >= vp->map_tile_cache->len)
				continue;
			if (map_vec.x < 0 || map_vec.y < 0)
				continue;
			if (map_vec.x >= map->w_by_tiles || map_vec.y >= map->h_by_tiles)
				continue;

			/* Draw map tile */
			mt = g_ptr_array_index (vp->map_tile_cache, tile_map_pos);
			update_and_draw_mt_if_not_moving (self, view_port_index, map, mt, map_vec);

			/* Draw sprites */
			for (guint i = 0; i < node->sprites->len; ++i)
			{
				if (engine_is_tile_visible_in_view_port (self, map_vec, view_port_index))
				{
					mt = g_ptr_array_index (node->sprites, i);
					update_and_draw_mt_if_not_moving (self, view_port_index, map, mt, map_vec);
				}
			}
		}
	}
}

void
engine_move_map_to_pos (Engine *self, guint view_port_index, Map *map, Vec2 new_map_pos, int vel)
{
	MovableTile *mt = NULL;
	Vec2 mt_new_pos = {0};
	ViewPort *vp = g_ptr_array_index (self->view_ports, view_port_index);

	vp->map_move_state = VIEWPORT_MAP_MOVING;

	g_assert (vp->map_tile_cache && vp->map_tile_cache->len > 0);

	for (guint i = 0; i < vp->map_tile_cache->len; ++i)
	{
		MapNode *node = NULL;
		mt = g_ptr_array_index (vp->map_tile_cache, i);
		Vec2 old_map_vec = engine_get_tile_map_pos (self, view_port_index, map, TILE(mt));

		mt_new_pos.x = TILE(mt)->pos.x - new_map_pos.x + vp->map_pos.x;
		mt_new_pos.y = TILE(mt)->pos.y - new_map_pos.y + vp->map_pos.y;

		movable_tile_move_to_pos (mt, mt_new_pos, vel);

		/* Move any sprite tiles */
		node = map_get_node_for_pos (map, old_map_vec);
		if (! node)
			continue;

		for (guint j = 0; j < node->sprites->len; ++j)
		{
			SpriteTile *st = g_ptr_array_index (node->sprites, j);

			if (engine_is_tile_visible_in_view_port (self, old_map_vec, view_port_index))
			{
				movable_tile_move_to_pos (MOVABLE_TILE(st), mt_new_pos, vel);
			}
		}
	}

	/* Only need to test this for the last tile in the loop since all map tiles move together */

	if (vec2_eq (TILE(mt)->pos, mt_new_pos))
	{
		vp->map_pos = new_map_pos;
		vp->map_move_state = VIEWPORT_MAP_STATIC;
	}
}

void
engine_move_map (Engine *self, guint view_port_index, Map *map, Vec2 vec, int vel)
{
	ViewPort *vp = g_ptr_array_index (self->view_ports, view_port_index);
	Vec2 new_map_pos = vec2_add (vp->map_pos, vec);

	engine_move_map_to_pos (self, view_port_index, map, new_map_pos, vel);
}

static bool
check_mt_with_map_tile_collision_absolute (Engine *self, guint view_port_index, Map *map, MovableTile *mt, Vec2 new_map_pos)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];
	bool retval = false;
	Tile *map_tile;
	CollisionType map_tile_coll;
	CollisionType mt_coll;
	int map_tile_index;

	/* Trying to go outside the bounds of the map counts as a collision
	 */
	if (new_map_pos.x < 0 || new_map_pos.x >= map->w_by_tiles || new_map_pos.y < 0 || new_map_pos.y >= map->h_by_tiles)
		return true;

	// FIXME - won't work once we only cache a portion of the map

	map_tile_index = util_get_1d_pos_from_2d_pos (new_map_pos.x, new_map_pos.y, map->w_by_tiles);

	map_tile = TILE(vp->map_tile_cache->pdata[map_tile_index]);

	map_tile_coll = g_array_index (map_tile->tileset->collision_matrix, int, map_tile->set_index);

	mt_coll = g_array_index (TILE(mt)->tileset->collision_matrix, int, TILE(mt)->set_index);

	retval = collision_util_compute_impact (mt_coll, map_tile_coll);

	return retval;
}

Vec2
engine_get_tile_map_pos (Engine *self, guint view_port_index, Map *map, Tile *tile)
{
	ViewPort *vp = g_ptr_array_index (self->view_ports, view_port_index);

	return vec2_add (tile->pos, vp->map_pos);
}

Vec2
engine_get_center_map_pos (Engine *self, guint view_port_index, Map *map)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];
	Vec2 vec;

	vec.x = MIN (vp->map_pos.x + vp->w_by_tiles / 2, vp->map_pos.x + (map->w_by_tiles - vp->map_pos.x) / 2);
	vec.y = MIN (vp->map_pos.y + vp->h_by_tiles / 2, vp->map_pos.y + (map->h_by_tiles - vp->map_pos.y) / 2);
	
	return vec;
}

void
engine_center_vp_around_player_tile (Engine *self, guint view_port_index, Map *map, PlayerTile *pt)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];
	Vec2 tile_map_pos;

	tile_map_pos = engine_get_tile_map_pos (self, view_port_index, map, TILE(pt));

	vp->map_pos.x = tile_map_pos.x - vp->w_by_tiles / 2;
	vp->map_pos.y = tile_map_pos.y - vp->h_by_tiles / 2;

	engine_set_tile_map_pos (self, view_port_index, map, TILE(pt), tile_map_pos);
}

void
engine_set_tile_map_pos (Engine *self, guint view_port_index, Map *map, Tile *tile, Vec2 new_map_vec)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];
	Vec2 new_pos = {0};

	new_pos.x = new_map_vec.x - vp->map_pos.x;
	new_pos.y = new_map_vec.y - vp->map_pos.y;

	tile_set_pos (tile, new_pos);
}

void
engine_set_sprite_tile_map_pos (Engine *self, guint view_port_index, Map *map, SpriteTile *st, Vec2 new_map_vec)
{
	Vec2 old_map_vec = engine_get_tile_map_pos (self, view_port_index, map, TILE(st));
	MapNode *old_node = map_get_node_for_pos (map, old_map_vec);
	MapNode *new_node = map_get_node_for_pos (map, new_map_vec);

	g_assert (new_node);

	engine_set_tile_map_pos (self, view_port_index, map, TILE(st), new_map_vec);

	if (old_node)
		map_node_remove_sprite_tile (old_node, st);

	if (new_node)
		map_node_add_sprite_tile (new_node, st);
}

bool
engine_check_mt_with_map_tile_collision (Engine *self, guint view_port_index, Map *map, MovableTile *mt, Vec2 vec)
{
	Vec2 old, new;

	old = engine_get_tile_map_pos (self, view_port_index, map, TILE(mt));
	new = vec2_add (old, vec);

	return check_mt_with_map_tile_collision_absolute (self, view_port_index, map, mt, new);
}

ViewPort *
engine_get_view_port (Engine *self, guint view_port_index)
{
	g_return_val_if_fail (view_port_index < self->view_ports->len, NULL);
	return g_ptr_array_index (self->view_ports, view_port_index);
}

bool
engine_is_tile_visible_in_view_port (Engine *self, Vec2 tile_map_pos, guint view_port_index)
{
	ViewPort *vp = self->view_ports->pdata[view_port_index];

	return
		tile_map_pos.x >=vp->map_pos.x						&&
		tile_map_pos.x <= vp->map_pos.x + vp->w_by_tiles	&&
		tile_map_pos.y >= vp->map_pos.y						&&
		tile_map_pos.y <= vp->map_pos.y + vp->h_by_tiles;
}

void
engine_move_player_tile_or_scroll_map (Engine *self, guint view_port_index, Map *map, PlayerTile *pt, Vec2 vec, int vel)
{
	ViewPort *vp = g_ptr_array_index (self->view_ports, view_port_index);
	Vec2 pt_maybe_new_pos = vec2_add (TILE(pt)->pos, vec);

	if (pt_maybe_new_pos.x >= vp->scroll_threshold					&&
		pt_maybe_new_pos.x <= vp->w_by_tiles - vp->scroll_threshold	&&
		pt_maybe_new_pos.y >= vp->scroll_threshold					&&
		pt_maybe_new_pos.y <= vp->h_by_tiles - vp->scroll_threshold)
	{
		movable_tile_move (MOVABLE_TILE(pt), vec, vel);
	}
	else
	{
		engine_move_map (self, view_port_index, map, vec, vel);
	}
}
