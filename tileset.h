// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "collision.h"

typedef enum
{
	TILESET_TYPE_INVALID = -1,
	TILESET_TYPE_BG_OUTDOOR = 0,
	TILESET_TYPE_BG_DUNGEON,
	TILESET_TYPE_SPRITE
} TileSetType;

typedef enum 
{
	TILESET_BG_OUTDOOR_INVALID = -1,
	TILESET_BG_OUTDOOR_GRASS = 0,
	TILESET_BG_OUTDOOR_WATER,
	TILESET_BG_OUTDOOR_NUM_TILES
} TileSetOutdoorTiles;

typedef enum 
{
	TILESET_BG_DUNGEON_INVALID = -1,
	TILESET_BG_DUNGEON_FLOOR = 0,
	TILESET_BG_DUNGEON_NUM_TILES
} TileSetDungeonTiles;

typedef enum 
{
	TILESET_SPRITE_TILE_INVALID = -1,
	TILESET_SPRITE_TILE_RIGHT_STATIC,
	TILESET_SPRITE_TILE_RIGHT_MOVE_1,
	TILESET_SPRITE_TILE_RIGHT_MOVE_LAST,
	TILESET_SPRITE_TILE_LEFT_STATIC,
	TILESET_SPRITE_TILE_LEFT_MOVE_1,
	TILESET_SPRITE_TILE_LEFT_MOVE_LAST,
	TILESET_SPRITE_TILE_DOWN_STATIC,
	TILESET_SPRITE_TILE_DOWN_MOVE_1,
	TILESET_SPRITE_TILE_DOWN_MOVE_LAST,
	TILESET_SPRITE_TILE_UP_STATIC,
	TILESET_SPRITE_TILE_UP_MOVE_1,
	TILESET_SPRITE_TILE_UP_MOVE_LAST,
	TILESET_SPRITE_NUM_TILES
} TileSetSpriteTiles;

#define TILESET(X) ((TileSet *)(X))

typedef struct TileSet TileSet;
struct TileSet
{
	GPtrArray *surfaces;
	GPtrArray *textures;
	GArray *collision_matrix;
	char *name;
};

TileSet *tileset_new (int num_tiles);
void tileset_constructor (TileSet *self, int num_tiles);
void tileset_destroy (TileSet *self);
void tileset_add_img (TileSet *self, guint index, const char *img_path, CollisionType type);
TileSet *tileset_create_from_file (const char *tset_path);
void tileset_get_sprite_tile_dir_indices (Vec2 dir_vec, int *start_index, int *end_index);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (TileSet, tileset_destroy)

/* SpriteTileSet */

#define SPRITE_TILESET(X) ((SpriteTileSet *)(X))

typedef struct SpriteTileSet SpriteTileSet;
struct SpriteTileSet
{
	TileSet parent;

	GArray *anim_speed_matrix;
};

SpriteTileSet *sprite_tileset_new (int num_tiles);
void sprite_tileset_destroy (SpriteTileSet *self);
