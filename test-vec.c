#include <stdio.h>
#include "vector.h"

void print_vec (Vec2 vec)
{
	printf ("(%d, %d)\n", vec.x, vec.y);
}

bool
test_vec2_size (void)
{
	Vec2 vec;
	bool retval;
	retval = (sizeof vec == 2 * sizeof (int));

	g_print ("%s: Ensuring size of vec2 == 2 * sizeof (int)\n", __func__);

	if (! retval)
	{
		g_printerr ("\tTest FAILED\n");
	}
	else
	{
		g_print ("\tTest PASSED\n");
	}
	return retval;
}

int main (void)
{
	test_vec2_size ();
	return 0;
}
