// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "tile.h"

/* Tile */

void
tile_constructor (Tile *self, TileSet *set, int tile_w, int tile_h)
{
	// FIXME - leak - we don't currently have a way of tracking how many tiles are using a given tileset, so we just set a non-owning pointer here which is never destroyed.
	self->tileset = set;
	self->pixel_pos.w = tile_w;
	self->pixel_pos.h = tile_h;
}

Tile *
tile_new (TileSet *set, int tile_w, int tile_h)
{
	Tile *self;

	g_assert (set && tile_w && tile_h);

	self = g_new0 (Tile, 1);
	tile_constructor (self, set, tile_w, tile_h);

	return self;
}

void
tile_destroy (Tile *self)
{
	g_free (self);
}

inline static void
tile_sync_pixel_pos_from_tile_pos (Tile *self)
{
	self->pixel_pos.x = self->pos.x * self->pixel_pos.w;
	self->pixel_pos.y = self->pos.y * self->pixel_pos.h;
}

void
tile_set_pos (Tile *self, Vec2 new_pos)
{
	self->pos.x = new_pos.x;
	self->pos.y = new_pos.y;
	tile_sync_pixel_pos_from_tile_pos (self);
}

/* MovableTile */

inline static void
movable_tile_sync_last_pixel_pos_with_cur_pos (MovableTile *self)
{
	self->last_pixel_pos = TILE(self)->pixel_pos;
}

void
movable_tile_constructor (MovableTile *self, TileSet *set, int tile_w, int tile_h)
{
	tile_constructor (&self->parent, set, tile_w, tile_h);
	movable_tile_sync_last_pixel_pos_with_cur_pos (self);
}

MovableTile *
movable_tile_new (TileSet *set, int tile_w, int tile_h)
{
	MovableTile *self;

	g_assert (set && tile_w && tile_h);

	self = g_new0 (MovableTile, 1);
	movable_tile_constructor (self, set, tile_w, tile_h);

	return self;
}

void
movable_tile_move (MovableTile *self, Vec2 vec, int vel)
{
	Vec2 new_pos = vec2_add (TILE(self)->pos, vec);
	movable_tile_move_to_pos (self, new_pos, vel);
}

void
movable_tile_move_to_pos (MovableTile *self, Vec2 new_pos, int vel)
{
	MovableTileMoveState new_state = MOVABLE_TILE_STATIC;
	int w = TILE(self)->pixel_pos.w;
	int h = TILE(self)->pixel_pos.h;

	SDL_Rect target_new_pos = {
		.x = new_pos.x * w,
		.y = new_pos.y * h,
		.w = w,
		.h = h
	};

	if (SDL_RectEquals (&TILE(self)->pixel_pos, &target_new_pos))
		goto out;

	if (new_pos.x > TILE(self)->pos.x) {
		TILE(self)->pixel_pos.x += vel;
		new_state = MOVABLE_TILE_MOVING;
	}
	else if (new_pos.x < TILE(self)->pos.x) {
		TILE(self)->pixel_pos.x -= vel;
		new_state = MOVABLE_TILE_MOVING;
	}

	if (new_pos.y > TILE(self)->pos.y) {
		TILE(self)->pixel_pos.y += vel;
		new_state = MOVABLE_TILE_MOVING;
	}
	else if (new_pos.y < TILE(self)->pos.y) {
		TILE(self)->pixel_pos.y -= vel;
		new_state = MOVABLE_TILE_MOVING;
	}

out:
	self->move_state = new_state;

	if (self->move_state == MOVABLE_TILE_STATIC)
	{
		TILE(self)->pos.x = new_pos.x;
		TILE(self)->pos.y = new_pos.y;
		movable_tile_sync_last_pixel_pos_with_cur_pos (self);
	}
}

void
movable_tile_destroy (MovableTile *self)
{
	/* Chain up */
	tile_destroy (&self->parent);
}

/* SpriteTile */

void
sprite_tile_constructor (SpriteTile *self, SpriteTileSet *set, int tile_w, int tile_h)
{
	movable_tile_constructor (&self->parent, TILESET(set), tile_w, tile_h);

	// FIXME/TODO - maybe not the worst default, but also kind of hard-code-y
	sprite_tile_set_direction (self, VEC2_DOWN);
}

SpriteTile *
sprite_tile_new (SpriteTileSet *set, int tile_w, int tile_h)
{
	SpriteTile *self;

	g_assert (set && tile_w && tile_h);

	self = g_new0 (SpriteTile, 1);
	sprite_tile_constructor (self, set, tile_w, tile_h);

	return self;
}

void
sprite_tile_destroy (SpriteTile *self)
{
	/* Chain up */
	movable_tile_destroy (&self->parent);
}

void
sprite_tile_set_direction (SpriteTile *self, Vec2 dir_vec)
{
	if (vec2_eq (dir_vec, self->dir_vec))
		return;

	if (vec2_eq (dir_vec, VEC2_LEFT))
	{
		if (MOVABLE_TILE(self)->move_state == MOVABLE_TILE_STATIC)
			TILE(self)->set_index = TILESET_SPRITE_TILE_LEFT_STATIC;
		else
			TILE(self)->set_index = TILESET_SPRITE_TILE_LEFT_MOVE_1;
	}
	else if (vec2_eq (dir_vec, VEC2_RIGHT))
	{
		if (MOVABLE_TILE(self)->move_state == MOVABLE_TILE_STATIC)
			TILE(self)->set_index = TILESET_SPRITE_TILE_RIGHT_STATIC;
		else
			TILE(self)->set_index = TILESET_SPRITE_TILE_RIGHT_MOVE_1;
	}
	else if (vec2_eq (dir_vec, VEC2_UP))
	{
		if (MOVABLE_TILE(self)->move_state == MOVABLE_TILE_STATIC)
			TILE(self)->set_index = TILESET_SPRITE_TILE_UP_STATIC;
		else
			TILE(self)->set_index = TILESET_SPRITE_TILE_UP_MOVE_1;
	}
	else if (vec2_eq (dir_vec, VEC2_DOWN))
	{
		if (MOVABLE_TILE(self)->move_state == MOVABLE_TILE_STATIC)
			TILE(self)->set_index = TILESET_SPRITE_TILE_DOWN_STATIC;
		else
			TILE(self)->set_index = TILESET_SPRITE_TILE_DOWN_MOVE_1;
	}
	else
	{
		g_autofree char *errmsg = g_strdup_printf ("%s: Invalid direction vector", __func__);
		FatalError (errmsg);
	}

	self->dir_vec = dir_vec;
}

void
sprite_tile_refresh_anim_speed (SpriteTile *self)
{
	Tile *tile = TILE(self);
	SpriteTileSet *sprite_tileset = SPRITE_TILESET(tile->tileset);

	self->anim_speed = g_array_index (sprite_tileset->anim_speed_matrix, int, tile->set_index);
}
