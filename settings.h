// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "input.h"

typedef struct Settings Settings;
struct Settings
{
	GKeyFile *keyfile;
	GHashTable *player_inputs;
};

Settings *settings_new (const char *ini_path);
void settings_destroy (Settings *self);
void settings_update (Settings *self);
InputTuple *settings_get_input_tuple_for_scancode (Settings *self, SDL_Scancode scancode);
