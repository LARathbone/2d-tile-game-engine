// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "player.h"

typedef enum
{
	VIEWPORT_MAP_STATIC,
	VIEWPORT_MAP_MOVING
} ViewPortMapMoveState;

typedef struct ViewPort ViewPort;
struct ViewPort
{
	int tile_w, tile_h;
	Vec2 map_pos;
	ViewPortMapMoveState map_move_state;
	GPtrArray *players;		/* not owned */
	GPtrArray *map_tile_cache;
	int w_by_tiles, h_by_tiles;
	SDL_Rect pixel_pos;
	int scroll_threshold;
};

ViewPort *view_port_new (int tile_w, int tile_h);
void view_port_destroy (ViewPort *self);


Player * view_port_get_controlling_player (ViewPort *self);
void view_port_add_player (ViewPort *self, Player *player);
void view_port_remove_player (ViewPort *self, Player *player);
bool view_port_check_player (ViewPort *self, Player *player);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ViewPort, view_port_destroy)
