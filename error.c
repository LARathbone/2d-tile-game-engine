// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "error.h"

void
PrintError (const char *errmsg)
{
	const char *sdlerr = SDL_GetError ();

	g_printerr ("Error:  %s\n", errmsg);
	if (sdlerr && strlen(sdlerr) > 0)
		g_printerr ("In addition, an SDL error was generated: %s\n", sdlerr);
}

void
FatalError (const char *errmsg)
{
	PrintError (errmsg);
	g_printerr ("Aborting.\n");
	exit (1);
}

