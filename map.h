// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "tile.h"
#include "util.h"

typedef struct MapNode MapNode;
struct MapNode
{
	int set_index;
	GPtrArray *sprites;
};

typedef struct Map Map;
struct Map
{
	GPtrArray *matrix;
	int w_by_tiles;
	int h_by_tiles;
};

/* MapNode */

MapNode *map_node_new (void);
void map_node_destroy (MapNode *self);
void map_node_add_sprite_tile (MapNode *self, SpriteTile *st);
void map_node_remove_sprite_tile (MapNode *self, SpriteTile *st);

/* Map */
Map *map_new (GPtrArray *matrix, int w_by_tiles, int h_by_tiles);
void map_set_matrix (Map *self, GPtrArray *matrix);
MapNode *map_get_node_for_pos (Map *self, Vec2 pos);

/* Util */

GPtrArray *map_util_create_empty_matrix (int w_by_tiles, int h_by_tiles);
GPtrArray *map_util_create_matrix_from_file (const char *path, int *w, int *h);
