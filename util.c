// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "util.h"

int
util_get_1d_pos_from_2d_pos (int xpos, int ypos, int w)
{
	return xpos + w * ypos;
}

void
util_get_2d_pos_from_2d_pos (int pos, int w, int *xpos, int *ypos)
{
	g_assert (w);

	*xpos = pos % w;
	*ypos = pos / w;
}
