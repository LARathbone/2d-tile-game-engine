// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "engine-signal.h"

inline static GArray *
create_queue (void)
{
	return g_array_new (TRUE, TRUE, sizeof (int));
}

EngineSignalStack *
engine_signal_stack_new (void)
{
	EngineSignalStack *self = g_new0 (EngineSignalStack, 1);

	self->queue = create_queue ();

	return self;
}

void
engine_signal_stack_destroy (EngineSignalStack *self)
{
	g_clear_pointer (&self->queue, g_array_unref);
	g_free (self);
}

void
engine_signal_stack_add (EngineSignalStack *self, EngineSignalType type)
{
	g_array_append_val (self->queue, type);
}

void
engine_signal_stack_clear (EngineSignalStack *self)
{
	g_clear_pointer (&self->queue, g_array_unref);
	self->queue = create_queue ();
}
