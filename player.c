// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "player.h"

Player *
player_new (void)
{
	Player *self = g_new0 (Player, 1);
	self->input = input_new ();

	return self;
}

void
player_destroy (Player *self)
{
	g_clear_pointer (&self->input, input_destroy);
	g_free (self);
}
