// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "settings.h"

#if 0
static int
player_input_compare (const char *str1, const char *str2)
{
	int pnum1, pnum2, ret;
	const char *player_str = "Player ";
	const int player_str_len = strlen (player_str);

	ret = sscanf (str1 + player_str_len-1, "%d", &pnum1);
	g_assert (ret == 1);

	ret = sscanf (str2 + player_str_len-1, "%d", &pnum2);
	g_assert (ret == 1);

	return pnum1 - pnum2;
}
#endif

static int
get_num_player_inputs (Settings *self)
{
	g_autoptr (GPtrArray) garr = g_ptr_array_new ();
	g_auto(GStrv) groups = g_key_file_get_groups (self->keyfile, NULL);
	const char *player_str = "Player ";
	const int player_str_len = strlen (player_str);

	for (int i = 0; groups[i] != NULL; ++i)
	{
		if (strncmp (groups[i], player_str, player_str_len) == 0)
			g_ptr_array_add (garr, g_steal_pointer (&groups[i]));
	}
	//g_ptr_array_sort (garr, player_input_compare);

	return garr->len;
}

static InputAction
action_from_str (const char *str)
{
	if (g_strcmp0 (str, "up") == 0)
		return DIR_UP;
	else if (g_strcmp0 (str, "down") == 0)
		return DIR_DOWN;
	else if (g_strcmp0 (str, "left") == 0)
		return DIR_LEFT;
	else if (g_strcmp0 (str, "right") == 0)
		return DIR_RIGHT;
	else
		return DIR_NONE;
}

static void
update_input_directions_for_player (Settings *self, int player_num)
{
	g_autofree char *key = g_strdup_printf ("Player %d", player_num);
	char *dirs[] = {"up", "down", "left", "right", NULL};

	g_assert (player_num > 0);

	for (int i = 0; dirs[i] != NULL; ++i)
	{
		InputAction action;
		InputTuple *tuple;
		int scancode;

		scancode = g_key_file_get_integer (self->keyfile, key, dirs[i], NULL);
		action = action_from_str (dirs[i]);
		tuple = input_tuple_new (player_num, action);

		g_hash_table_replace (self->player_inputs, GINT_TO_POINTER (scancode), tuple);
	}
}

static void
update_input_directions_for_all_players (Settings *self)
{
	int num = get_num_player_inputs (self);
	g_assert (num > 0);

	for (int i = 1; i <= num; ++i)
	{
		update_input_directions_for_player (self, i);
	}
}

Settings *
settings_new (const char *ini_path)
{
	Settings *self = g_new0 (Settings, 1);
	g_autoptr(GError) local_error = NULL;

	self->player_inputs = g_hash_table_new_full (NULL, NULL, NULL, (GDestroyNotify)input_tuple_destroy);

	self->keyfile = g_key_file_new ();
	g_key_file_load_from_file (self->keyfile, ini_path, G_KEY_FILE_NONE, &local_error);
	if (local_error) FatalError (local_error->message);

	settings_update (self);

	return self;
}

void
settings_destroy (Settings *self)
{
	g_clear_pointer (&self->player_inputs, g_hash_table_unref);
	g_clear_pointer (&self->keyfile, g_key_file_unref);
	g_free (self);
}

void
settings_update (Settings *self)
{
	update_input_directions_for_all_players (self);
}

InputTuple *
settings_get_input_tuple_for_scancode (Settings *self, SDL_Scancode scancode)
{
	return g_hash_table_lookup (self->player_inputs, GINT_TO_POINTER (scancode));
}






