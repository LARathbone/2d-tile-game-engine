// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#include "viewport.h"

#define VP_DEFAULT_SCROLL_THRESHOLD 4

ViewPort *
view_port_new (int tile_w, int tile_h)
{
	ViewPort *self = g_new0 (ViewPort, 1);

	self->tile_w = tile_w;
	self->tile_h = tile_h;
	self->players = g_ptr_array_new ();
	self->scroll_threshold = VP_DEFAULT_SCROLL_THRESHOLD;

	return self;
}

void
view_port_destroy (ViewPort *self)
{
	g_clear_pointer (&self->map_tile_cache, g_ptr_array_unref);
	g_clear_pointer (&self->players, g_ptr_array_unref);
	g_free (self);
}

Player *
view_port_get_controlling_player (ViewPort *self)
{
	if (self->players->len == 1)
		return self->players->pdata[0];

	/* If anything other than one player is within the viewport, no one is the controlling player.
	 */
	return NULL;
}

void
view_port_add_player (ViewPort *self, Player *player)
{
	g_ptr_array_add (self->players, player);
}

void
view_port_remove_player (ViewPort *self, Player *player)
{
	g_ptr_array_remove (self->players, player);
}

bool
view_port_check_player (ViewPort *self, Player *player)
{
	return g_ptr_array_find (self->players, player, NULL);
}
