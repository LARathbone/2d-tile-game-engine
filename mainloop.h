// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak number

#pragma once

#include "common-headers.h"
#include "engine.h"
#include "game-state.h"
#include "timer.h"

void MainLoop (GameState *state);
